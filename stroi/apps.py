from django.apps import AppConfig


class StroiConfig(AppConfig):
    name = 'stroi'
