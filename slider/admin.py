from django.contrib import admin

# Register your models here.
from django.contrib import admin
from django import forms
from .models import Slider, AdPhoto


class ViewSliderAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ViewSliderAdminForm, self).__init__(*args, **kwargs)

        self.fields['photo'].label = 'Фото'
        self.fields['desc'].label = 'Описание'
        self.fields['url'].label = 'Ссылка'
        self.fields['isActive'].label = 'Статус'


class ViewSlider(admin.ModelAdmin):
    list_display = ["image_tag",  "fdesc", "furl", "fisActive"]

    def fdesc(self, obj):
        return obj.desc

    fdesc.short_description = 'Описание'

    def furl(self, obj):
        return obj.url

    furl.short_description = 'Ссылка'

    def fisActive(self, obj):
        return obj.isActive

    fisActive.short_description = 'Статус'

    search_fields = ['desc']
    form = ViewSliderAdminForm


class ViewAdAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ViewAdAdminForm, self).__init__(*args, **kwargs)

        self.fields['photo'].label = 'Фото'
        self.fields['desc'].label = 'Описание'
        self.fields['url'].label = 'Ссылка'
        self.fields['isActive'].label = 'Статус'


class ViewAd(admin.ModelAdmin):
    list_display = ["image_tag",  "fdesc", "furl", "fisActive"]

    def fdesc(self, obj):
        return obj.desc

    fdesc.short_description = 'Описание'

    def furl(self, obj):
        return obj.url

    furl.short_description = 'Ссылка'

    def fisActive(self, obj):
        return obj.isActive

    fisActive.short_description = 'Статус'

    search_fields = ['desc']
    form = ViewAdAdminForm


admin.site.register(Slider, ViewSlider),
admin.site.register(AdPhoto, ViewAd),


