import copy

import PIL
from PIL import Image
from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.utils.safestring import mark_safe

# Create your models here.
from for_image import give_name, resize_image
# for deleting
from django.db.models.signals import *
from django.dispatch import receiver


class Slider(models.Model):
    photo = models.ImageField(max_length=1000)
    desc = models.CharField(max_length=255, default='opisanie for slider')
    url = models.URLField(max_length=50)

    isActive = models.BooleanField(default=True)

    keyword = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = _("Слайдеры")
        verbose_name_plural = _("Слайдер")

    def save(self, *args, **kwargs):

        self.photo.name = give_name(self.photo.name, 'slider/848')

        super(Slider, self).save(*args, **kwargs)

        img1 = Image.open(self.photo)

        img1 = img1.resize((848, 486), PIL.Image.ANTIALIAS)
        img1.save(self.photo.path)

    def image_tag(self):
        return mark_safe('<img src="%s" width="150" " />' % self.photo.url)

    image_tag.short_description = 'Изображение'


@receiver(post_delete, sender=Slider)
def submission_delete(sender, instance, **kwargs):
    instance.photo.delete(False)


class AdPhoto(models.Model):
    photo = models.ImageField(max_length=1000)
    desc = models.CharField(max_length=255)
    url = models.URLField(max_length=50)
    isActive = models.BooleanField(default=True)

    keyword = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = _("Фото-реклама")
        verbose_name_plural = _("Фото-рекламы")

    def save(self, *args, **kwargs):

        self.photo.name = give_name(self.photo.name, 'advertising/ad')

        super(AdPhoto, self).save(*args, **kwargs)

    def image_tag(self):
        return mark_safe('<img src="%s" width="150" " />' % self.photo.url)

    image_tag.short_description = 'Изображение'


@receiver(post_delete, sender=AdPhoto)
def submission_delete(sender, instance, **kwargs):
    instance.photo.delete(False)
