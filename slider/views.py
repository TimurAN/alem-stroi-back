import json
from django.core import serializers

from django.http import JsonResponse

from .models import Slider, AdPhoto


def list_slider_photo(request):
    slider = Slider.objects.all()
    json_slider = serializers.serialize('json', slider)
    json_slider = json.loads(json_slider)

    return JsonResponse({'slider_photos': json_slider})


def list_advertising_photo(request):
    slider = AdPhoto.objects.filter(isActive=True).order_by('-pk')
    json_slider = serializers.serialize('json', slider)
    json_slider = json.loads(json_slider)[0:2]

    return JsonResponse({'advertising_photos': json_slider})
