from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [
    path('', list_only_category),
    path('all/', list_category),
    path('<int:category_id>', get_by_category)

]