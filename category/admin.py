from django.contrib import admin
from django import forms
from .models import Category, SubCategory, Sub2Category


class ViewCategoryAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ViewCategoryAdminForm, self).__init__(*args, **kwargs)

        self.fields['title'].label = 'Название'
        self.fields['isActive'].label = 'Активность'


class ViewCategory(admin.ModelAdmin):
    list_display = ["ftitle", "fisActive"]

    def ftitle(self, obj):
        return obj.title

    ftitle.short_description = 'НАЗВАНИЕ'

    def fisActive(self, obj):
        return obj.isActive

    fisActive.short_description = 'Активность'

    search_fields = ['title']
    form = ViewCategoryAdminForm


class ViewSubCategoryAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ViewSubCategoryAdminForm, self).__init__(*args, **kwargs)

        self.fields['photo'].label = 'Фото'
        self.fields['title'].label = 'Название'
        self.fields['isActive'].label = 'Активность'
        self.fields['category'].label = 'Категория'




class ViewSubCategory(admin.ModelAdmin):
    list_display = ["image_tag", "ftitle", "fcategory"]

    def ftitle(self, obj):
        return obj.title

    ftitle.short_description = 'НАЗВАНИЕ'

    def fcategory(self, obj):
        return obj.category

    fcategory.short_description = 'Описание'

    search_fields = ['title']
    form = ViewSubCategoryAdminForm


class ViewSub2CategoryAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ViewSub2CategoryAdminForm, self).__init__(*args, **kwargs)

        self.fields['photo'].label = 'Фото'
        self.fields['title'].label = 'Название'
        self.fields['isActive'].label = 'Активность'
        self.fields['category'].label = 'Категория'
        self.fields['subCategory'].label = 'ПодКатегория'


class ViewSub2Category(admin.ModelAdmin):
    list_display = ["image_tag", "ftitle", "fsubcategory"]

    def ftitle(self, obj):
        return obj.title

    ftitle.short_description = 'НАЗВАНИЕ'

    def fsubcategory(self, obj):
        return obj.subCategory

    fsubcategory.short_description = 'Описание'

    search_fields = ['title']
    form = ViewSub2CategoryAdminForm


admin.site.register(Category, ViewCategory),
admin.site.register(SubCategory, ViewSubCategory),
admin.site.register(Sub2Category, ViewSub2Category),
