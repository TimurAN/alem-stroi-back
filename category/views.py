import json
from django.core import serializers
from category.models import Category, SubCategory
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render

# Create your views here.
from product.models import Product


def list_category(request):
    categories = Category.objects.all()
    json_categories = serializers.serialize('json', categories)
    json_categories = json.loads(json_categories)

    i = 0
    for category in categories:
        sub_categories = category.subcategories.all()
        json_sub_categories = serializers.serialize('json', sub_categories)
        json_sub_categories = json.loads(json_sub_categories)
        json_categories[i]['subcategories'] = json_sub_categories

        j = 0
        for sc in sub_categories:
            sub2_categories = sc.sub2categories.all()
            json_sub2_categories = serializers.serialize('json', sub2_categories)
            json_sub2_categories = json.loads(json_sub2_categories)
            json_categories[i]['subcategories'][j]['subcategories'] = json_sub2_categories
            j += 1
        i += 1

    return JsonResponse({'categories': json_categories})


def list_only_category(request):
    categories = Category.objects.all()
    json_categories = serializers.serialize('json', categories)
    json_categories = json.loads(json_categories)
    return JsonResponse({'categories': json_categories})


def get_by_category(request, category_id):
    sub_categories = SubCategory.objects.filter(category_id=category_id)
    products = Product.objects.filter(category=category_id).order_by('-createDate')

    j_sub_categories = serializers.serialize('json', sub_categories)
    j_sub_categories = json.loads(j_sub_categories)

    json_products = serializers.serialize('json', products)
    json_products = json.loads(json_products)

    return JsonResponse({'sub_categories': j_sub_categories, 'products': json_products})
