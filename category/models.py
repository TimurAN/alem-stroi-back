from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from numpy.core import unicode

from for_image import give_name, resize_image

# for deleting
from django.db.models.signals import *
from django.dispatch import receiver
from smart_selects.db_fields import *
import copy


class Category(models.Model):
    title = models.CharField(max_length=50)
    isActive = models.BooleanField(default=True)
    keyword = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    footer_status = models.BooleanField(blank=True, default=False)

    def __str__(self):
        return self.title

    def __unicode__(self):
        return unicode(self.title)

    class Meta:
        verbose_name = _("Категория")
        verbose_name_plural = _("Категории")


#########################################################################################################

class SubCategory(models.Model):
    title = models.CharField(max_length=50)
    isActive = models.BooleanField(default=True)
    keyword = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    photo = models.ImageField(max_length=1000)
    photo_small = models.ImageField(blank=True, null=True, max_length=1000, editable=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='subcategories')

    def __unicode__(self):
        return unicode(self.title)

    def save(self, *args, **kwargs):
        self.photo_small = copy.deepcopy(self.photo)

        self.photo_small.name = give_name(self.photo_small.name, 'subCategory/170')
        self.photo.name = give_name(self.photo.name, 'subCategory/700')

        super(SubCategory, self).save(*args, **kwargs)

        resize_image(self, 700, 170)

    def image_tag(self):
        return mark_safe('<img src="%s" width="130" height="100" " />' % self.photo.url)

    image_tag.short_description = 'Изображение'

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Под1категория")
        verbose_name_plural = _("Под1категории")


@receiver(post_delete, sender=SubCategory)
def submission_delete(sender, instance, **kwargs):
    instance.photo.delete(False)
    instance.photo_small.delete(False)


#########################################################################################################

class Sub2Category(models.Model):
    title = models.CharField(max_length=50)
    isActive = models.BooleanField(default=True)
    keyword = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    photo = models.ImageField(max_length=1000)
    photo_small = models.ImageField(blank=True, null=True, max_length=1000, editable=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)
    subCategory = ChainedForeignKey(
        SubCategory,
        on_delete=models.CASCADE,
        related_name='sub2categories',
        chained_field="category",
        chained_model_field="category",
        show_all=False,
        auto_choose=True,
    )

    def save(self, *args, **kwargs):
        self.photo_small = copy.deepcopy(self.photo)

        self.photo_small.name = give_name(self.photo_small.name, 'sub2Category/170')
        self.photo.name = give_name(self.photo.name, 'sub2Category/700')

        super(Sub2Category, self).save(*args, **kwargs)

        resize_image(self, 700, 170)

    def image_tag(self):
        return mark_safe('<img src="%s" width="130" height="100" " />' % self.photo.url)

    image_tag.short_description = 'Изображение'

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Под2категория")
        verbose_name_plural = _("Под2категории")


@receiver(post_delete, sender=Sub2Category)
def submission_delete(sender, instance, **kwargs):
    instance.photo.delete(False)
    instance.photo_small.delete(False)
