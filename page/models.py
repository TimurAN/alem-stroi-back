from django.db import models
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.translation import ugettext_lazy as _


class Page(models.Model):
    title = models.CharField(max_length=255)
    slug = models.CharField(max_length=255, unique=True)
    text = RichTextUploadingField(default=title)
    isActive = models.BooleanField(default=False)
    keyword = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)



    class Meta:
        verbose_name = _("Страница")
        verbose_name_plural = _("Страницы")
