from django.contrib import admin

# Register your models here.
from django.contrib import admin
from django import forms
from .models import Page


class ViewPageAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ViewPageAdminForm, self).__init__(*args, **kwargs)

        self.fields['title'].label = 'Название'
        self.fields['isActive'].label = 'Активность'


class ViewPage(admin.ModelAdmin):
    list_display = ["ftitle", "fisActive"]

    def ftitle(self, obj):
        return obj.title

    ftitle.short_description = 'НАЗВАНИЕ'

    def fisActive(self, obj):
        return obj.isActive

    fisActive.short_description = 'Активность'

    search_fields = ['title']
    form = ViewPageAdminForm


admin.site.register(Page, ViewPage),
