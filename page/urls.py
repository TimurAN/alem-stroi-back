from django.urls import path
from .views import *


urlpatterns = [
    path('', index),
    path('page/<str:slug>', get_page)
]