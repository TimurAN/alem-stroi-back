import json

from django.http import JsonResponse
from django.shortcuts import render
from django.core import serializers
from .models import Page


# Create your views here.
def index(request):
    return render(request, 'page/index.html')


def get_page(request, slug):
    page = Page.objects.filter(slug=slug).first()

    if not page:
        page = Page.objects.create(slug=slug, title=slug, text="new text")

    page = [page]
    json_page = serializers.serialize('json', page)
    json_page = json.loads(json_page)[0]


    json_page['fields']['text'] = json_page['fields']['text'].replace('src="/media/uploads/', 'src="http://a0310335.xsph.ru/media/uploads/')


    return JsonResponse({'page': json_page})
