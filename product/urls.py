from django.urls import path
from . import views


urlpatterns = [
    path('', views.list_product),
    path('<int:id>', views.one_product),
    path('carts/', views.list_cart),
    path('add/cart', views.new_cart),
    path('limit/<int:offset>/<int:limit>', views.limit_product),
    path('hit/<int:offset>/<int:limit>', views.hit_product),
    path('stock/<int:offset>/<int:limit>', views.stock_product),
    path('by/category/<int:category>/<int:sub_category>/<int:sub2_category>/<int:offset>/<int:limit>', views.get_category_product),
    path('search/<int:category_id>/<str:text>', views.search_product),

]