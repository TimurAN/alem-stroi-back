import json
from django.core import serializers

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from category.models import SubCategory, Sub2Category, Category
from product.models import Product, Cart, CartProduct

from django.core.mail import send_mail

from django.core.mail.message import EmailMessage


def list_product(request):
    products = Product.objects.all().order_by('-createDate')
    json_products = serializers.serialize('json', products)
    json_products = json.loads(json_products)

    return JsonResponse({'products': json_products})


def one_product(request, id):
    product = Product.objects.filter(id=id)

    json_product = serializers.serialize('json', product)
    json_product = json.loads(json_product)[0]
    return JsonResponse({'product': json_product})


def list_cart(request):
    carts = Cart.objects.all()
    json_carts = serializers.serialize('json', carts)
    json_carts = json.loads(json_carts)

    return JsonResponse({'carts': json_carts})


def limit_product(request, offset, limit):
    products = Product.objects.order_by('-createDate')[offset:offset + limit]
    json_products = serializers.serialize('json', products)
    json_products = json.loads(json_products)

    return JsonResponse({'products': json_products})


def get_category_product(request, category, sub_category, sub2_category, offset, limit):
    size = None

    if sub2_category != 0:
        products = Product.objects.filter(sub2Category=sub2_category).order_by('-createDate')[offset:offset + limit]
        categ = Sub2Category.objects.filter(id=sub2_category).first()
        if offset == 0:
            size = len(Product.objects.filter(sub2Category=sub2_category).all())
        subCategories = []

    elif sub_category != 0:
        products = Product.objects.filter(subCategory=sub_category).order_by('-createDate')[offset:offset + limit]
        subCategories = Sub2Category.objects.filter(subCategory=sub_category)
        categ = SubCategory.objects.filter(id=sub_category).first()
        if offset == 0:
            size = len(Product.objects.filter(subCategory=sub_category).all())

    else:
        products = Product.objects.filter(category=category).order_by('-createDate')[offset:offset + limit]
        subCategories = SubCategory.objects.filter(category=category)
        categ = Category.objects.filter(id=category).first()
        if offset == 0:
            size = len(Product.objects.filter(category=category).all())

    if len(products) != 0:
        products = serializers.serialize('json', products)
        products = json.loads(products)
    else:
        products = None

    if len(subCategories) != 0:
        subCategories = serializers.serialize('json', subCategories)
        subCategories = json.loads(subCategories)
    else:
        subCategories = None

    return JsonResponse({'products': products, 'size': size, 'subcategories': subCategories, 'title': categ.title})


def search_product(request, category_id, text):
    if category_id == 0:
        products = Product.objects.all()
    else:
        products = Product.objects.filter(category=category_id).order_by('-createDate')

    if text != 'null':
        products = products.filter(title__icontains=text).order_by('-createDate') | products.filter(
            text__icontains=text).order_by('-createDate')
    json_products = serializers.serialize('json', products)
    json_products = json.loads(json_products)

    return JsonResponse({'products': json_products})


def hit_product(request, offset, limit):
    products = Product.objects.filter(hit=True).order_by('-createDate')[offset:offset + limit]
    json_products = serializers.serialize('json', products)
    json_products = json.loads(json_products)

    return JsonResponse({'products': json_products})


def stock_product(request, offset, limit):
    products = Product.objects.filter(stock=True).order_by('-percent')[offset:offset + limit]
    json_products = serializers.serialize('json', products)
    json_products = json.loads(json_products)

    return JsonResponse({'products': json_products})


@csrf_exempt
def new_cart(request):
    if request.method == 'POST':
        try:
            cart = json.loads(request.body)
        except:
            return JsonResponse({'message': 'object is empty'})

        cart = cart['cart']
        products = cart['products']
        pi = cart['personal_info']

        if pi['picked'] == 'Доставка':
            cp = Cart.objects.create(
                clientName=pi['clientName'],
                phoneNumber=pi['tel'],
                address=pi['address'],
                comment=pi['comment'],
                picked=pi['picked'],
                deliveryDate=pi['date'],
                time=pi['time'],
            )
        else:
            cp = Cart.objects.create(
                clientName=pi['clientName'],
                phoneNumber=pi['tel'],
                address=pi['address'],
                comment=pi['comment'],
                picked=pi['picked'],
                deliveryDate='--------',
                time='--------',

            )

        mesPI = \
            '''
            <h1 style="text-align:center"><span style="font-size:26px"><strong><span style="font-family:Georgia,serif"><cite>Новый заказ:</cite></span></strong></span></h1>

            <h4><u><em><strong>Данные клиента:</strong></em></u></h4>
            
            <table border="1" cellpadding="4" cellspacing="0" style="width:500px">
                <tbody>
                    <tr>
                        <td>ФИО</td>
                        <td>{}</td>
                    </tr>
                    <tr>
                        <td>Номер телефона</td>
                        <td>{}</td>
                    </tr>
                    <tr>
                        <td>Адрес</td>
                        <td>{}</td>
                    </tr>
                </tbody>
            </table>
            <h4><u><em><strong>Комментарии:</strong></em></u></h4>
           
            <p>{}</p>
            
                        
            '''.format(pi['clientName'], pi['tel'], pi['address'], pi['comment'])

        mesCartHeader = \
            '''
                <h4><u><em><strong>Корзина</strong></em></u>:</h4>

                <table border="1" cellpadding="4" cellspacing="0" style="width:500px">
                    <tbody>
                        <tr>
                            <td>№</td>
                            <td>Наименование товара</td>
                            <td>Цена(ед)</td>
                            <td>Количество</td>
                            <td>Вес</td>
                            <td>Сумма</td>
                            <td>Суммарный вес</td>
                        </tr>
            '''
        mesCartBody = ''
        mesCartFooter = \
            '''                        
                    </tbody>
                </table>

            '''

        sumPrice = 0
        sumWeight = 0

        hys = "{}\t{}\t{}\t{}\n".format('Название', 'Количество', 'Цена', 'Сумма')
        hys += '_' * 50 + '\n'
        count = 0
        for product in products:
            count += 1
            pr = Product.objects.filter(id=product['pk'])
            if not pr:
                return JsonResponse({'message': "product not found"})
            pr = pr[0]
            hys += "\t{}\t{}\t{}\t{}\n".format(pr.title, product['amount'], pr.price, product['amount'] * pr.price)
            mesCartBody += '<tr><td>{}</td><td>{}</td><td>{}</td><td>{}</td><td>{}</td><td>{}</td><td>{}</td></tr>'.format(
                count,
                pr.title,
                pr.price,
                product['amount'],
                pr.weight,
                product['amount'] * pr.price,
                product['amount'] * pr.weight

            )
            sumPrice += product['amount'] * pr.price
            sumWeight += product['amount'] * pr.weight

            CartProduct.objects.create(
                cart=cp,
                product=pr,
                qty=product['amount']
            )

        hys += '_' * 50 + '\n'
        hys += 'Суммарная цена:{}\t\n'.format(sumPrice)
        hys += 'Суммарный вес:{}\t\n'.format(sumWeight)

        cp.history = hys
        cp.save()

        mesResult = \
            '''
            <h4><u><em><strong>Итог:</strong></em></u></h4>
    
            <table border="1" cellpadding="4" cellspacing="0" style="width:500px">
                <tbody>
                    <tr>
                        <td>
                        <p>Общая сумма:&nbsp;</p>
                        </td>
                        <td>{}</td>
                    </tr>
                    <tr>
                        <td>
                        <p>Общий вес:</p>
                        </td>
                        <td>{}</td>
                    </tr>
                    <tr>
                        <td>
                        <p>Доставка:</p>
                        </td>
                        <td>{}</td>
                    </tr>
                    <tr>
                        <td>
                        <p>Дата:</p>
                        </td>
                        <td>{}</td>
                    </tr>
                    <tr>
                        <td>
                        <p>Время:</p>
                        </td>
                        <td>{}</td>
                    </tr>
                </tbody>
            </table>
    
            '''.format(sumPrice, sumWeight, cp.picked, cp.deliveryDate, cp.time)

        mes = mesPI + mesCartHeader + mesCartBody + mesCartFooter + mesResult

        #######################Sending Email###############################
        subject = 'Новый заказ'
        sending_email(mes, subject)

        return JsonResponse({'message': "successfully added"})
    else:
        return JsonResponse({'message': 'chose another request method'})


def sending_email(mes, subject):
    email_address = ['abdymazhinov.timur@gmail.com', 'kalmanbetovaman@gmail.com']
    email = EmailMessage()
    email.subject = subject

    email.from_email = "ALEMSTROI <alemstroi.ru@gmail.com>"
    email.to = email_address
    email.attach(content=mes, mimetype="text/html")

    email.send()
