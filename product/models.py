import copy

from ckeditor_uploader.fields import RichTextUploadingField
from django.core.validators import MinValueValidator
from django.db import models
# for deleting
from django.db.models.signals import *
from django.dispatch import receiver
from django.utils import timezone
from django.utils.safestring import mark_safe

from category.models import Category, SubCategory, Sub2Category

from django.utils.translation import ugettext_lazy as _
from smart_selects.db_fields import *

from category.models import Category, SubCategory
from for_image import give_name, resize_image


class Product(models.Model):
    title = models.CharField(max_length=100)
    text = RichTextUploadingField()
    price = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    keyword = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    hit = models.BooleanField(default=False)
    stock = models.BooleanField(default=False)
    percent = models.IntegerField(default=0, blank=True)
    createDate = models.DateTimeField(default=timezone.now, editable=False)
    isPicked = models.BooleanField(default=False, editable=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    subCategory = ChainedForeignKey(
        SubCategory,
        chained_field="category",
        chained_model_field="category",
        show_all=False,
        auto_choose=True,
    )
    sub2Category = ChainedForeignKey(
        Sub2Category,
        chained_field="subCategory",
        chained_model_field="subCategory",
        show_all=False,
        auto_choose=True,
        on_delete=models.CASCADE,
        blank=True,
        null=True)

    photo = models.ImageField(max_length=1000)
    photo_small = models.ImageField(blank=True, null=True, max_length=1000, editable=False)
    weight = models.DecimalField(max_digits=5, decimal_places=2, null=True, default=0.0)

    class Meta:
        verbose_name = _("Продукт")
        verbose_name_plural = _("Продукты")

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.photo_small = copy.deepcopy(self.photo)
        self.photo_small.name = give_name(self.photo_small.name, 'product/170')
        self.photo.name = give_name(self.photo.name, 'product/700')

        super(Product, self).save(*args, **kwargs)
        resize_image(self, 700, 170)

    def image_tag(self):
        return mark_safe('<img src="%s" width="150" " />' % self.photo_small.url)

    image_tag.short_description = 'Изображение'


@receiver(post_delete, sender=Product)
def submission_delete(sender, instance, **kwargs):
    instance.photo.delete(False)
    instance.photo_small.delete(False)


class Cart(models.Model):
    clientName = models.CharField(max_length=255)
    phoneNumber = models.CharField(max_length=20)
    address = models.TextField()
    comment = models.TextField()
    createDate = models.DateTimeField(default=timezone.now, editable=False)
    status = models.CharField(max_length=30)
    history = models.TextField(null=True)
    picked = models.CharField(max_length=30, null=True)
    deliveryDate = models.CharField(max_length=30, null=True)
    time = models.CharField(max_length=50, null=True)

    class Meta:
        verbose_name = _("Корзина")
        verbose_name_plural = _("Корзины")


class CartProduct(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    qty = models.PositiveIntegerField(validators=[MinValueValidator(1)])
