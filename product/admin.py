from django.contrib import admin
from .models import Product, Cart
from category.models import Category, SubCategory
from django import forms


# Register your models here.


class ProductAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProductAdminForm, self).__init__(*args, **kwargs)

        self.fields['title'].label = 'Название товара'
        self.fields['text'].label = 'Описание товара'
        self.fields['price'].label = 'Цена'
        self.fields['photo'].label = 'Изображение'
        self.fields['category'].label = 'Категории'
        self.fields['subCategory'].label = 'Под1Категории'
        self.fields['sub2Category'].label = 'Под2Категории'

        self.fields['hit'].label = 'Хит'
        self.fields['stock'].label = 'Акция'
        self.fields['percent'].label = 'Процент(Акция)'
        self.fields['weight'].label = 'вес(кг)'


class ViewProduct(admin.ModelAdmin):
    list_display = (
    'image_tag', 'ftitle', 'ftext', 'fprice', 'fcategory', 'fsubCategory', 'fsub2Category', 'fhit', 'fstock', 'fpercent', 'fweight')
    save_as = True

    def ftitle(self, obj):
        return obj.title
    ftitle.short_description = 'НАЗВАНИЕ'

    def ftext(self, obj):
        return obj.text
    ftext.short_description = 'ОПИСАНИЕ'

    def fprice(self, obj):
        return obj.price
    fprice.short_description = 'ЦЕНА'

    def fcategory(self, obj):
        return obj.category
    fcategory.short_description = 'КАТЕГОРИИ'

    def fsubCategory(self, obj):
        return obj.subCategory
    fsubCategory.short_description = 'ПОД1КАТЕГОРИИ'

    def fsub2Category(self, obj):
        return obj.sub2Category
    fsub2Category.short_description = 'ПОД2КАТЕГОРИИ'


    def fhit(self, obj):
        return obj.hit
    fhit.short_description = 'ХИТ'

    def fstock(self, obj):
        return obj.stock
    fstock.short_description = 'АКЦИЯ'

    def fpercent(self, obj):
        return obj.percent
    fpercent.short_description = 'ПРОЦЕНТ'

    def fweight(self, obj):
        return obj.weight
    fweight.short_description = 'ВЕС'

    readonly_fields = ['image_tag']
    search_fields = ["title"]

    #############Категория жана Подкатегория __Selectorлору  ########################################################

    def formfield_for_foreignkey(self, db_field, request, **kwargs):

        if db_field.name == "category":
            kwargs["queryset"] = Category.objects.all()

        if db_field.name == "subCategory":
            kwargs["queryset"] = SubCategory.objects.all()

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    form = ProductAdminForm


class ViewCartAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ViewCartAdminForm, self).__init__(*args, **kwargs)

        self.fields['clientName'].label = 'Имя клиента'
        self.fields['phoneNumber'].label = 'Номер телефона'
        self.fields['address'].label = 'Адрес'
        self.fields['comment'].label = 'Комментарии'
        self.fields['createDate'].label = 'Время создания'
        self.fields['status'].label = 'Статус'


class ViewCart(admin.ModelAdmin):
    list_display = ["fclientName", "fphoneNumber", "fcomment", "fstatus", "fhistory"]

    def fclientName(self, obj):
        return obj.clientName

    fclientName.short_description = 'Имя клиента'

    def fphoneNumber(self, obj):
        return obj.phoneNumber

    fphoneNumber.short_description = 'Номер телефона'

    def fcomment(self, obj):
        return obj.comment

    fcomment.short_description = 'Описание'

    def fstatus(self, obj):
        return obj.status

    fstatus.short_description = 'Статус'

    def fhistory(self, obj):
        return obj.history

    fhistory.short_description = 'История'

    search_fields = ["clientName"]
    form = ViewCartAdminForm


class ViewCartProduct(admin.ModelAdmin):
    list_display = ["clientName", "phoneNumber", "comment", "status"]
    search_fields = ["clientName"]


admin.site.register(Product, ViewProduct),
admin.site.register(Cart, ViewCart),
