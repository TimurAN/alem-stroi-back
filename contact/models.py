from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from django.core.mail.message import EmailMessage
from html2text import *

# Create your models here.z


class Contact(models.Model):
    phoneNumber = models.CharField(max_length=20)
    phoneNumber2 = models.CharField(max_length=20, null=True, blank=True)
    phoneNumber3 = models.CharField(max_length=20, null=True, blank=True)
    whatsApp = models.CharField(max_length=20, null=True)
    vk_url = models.URLField(null=True, blank=True)
    ok_url = models.URLField(null=True, blank=True)
    instagram = models.URLField(null=True, blank=True)
    address = models.CharField(max_length=100)
    email = models.CharField(max_length=50)

    class Meta:
        verbose_name = _("Контакт")
        verbose_name_plural = _("Контакты")


class CallMe(models.Model):
    name = models.CharField(max_length=100)
    phoneNumber = models.CharField(max_length=20)
    createDate = models.DateTimeField(default=timezone.now)
    callDate = models.CharField(max_length=50)

    class Meta:
        verbose_name = _("Обратная связь")
        verbose_name_plural = _("Обратные связи")


class Email(models.Model):
    email = models.EmailField(unique=True)

    class Meta:
        verbose_name = _("Эл.почта")
        verbose_name_plural = _("Эл.почты")


class Distribution(models.Model):
    title = models.CharField(max_length=255, default='This is title')
    text = RichTextUploadingField(default=title)
    file = models.FileField(upload_to='file/', blank=True, null=True)

    class Meta:
        verbose_name = _("Рассылка")
        verbose_name_plural = _("Рассылки")

    def save(self, *args, **kwargs):
        super(Distribution, self).save(*args, **kwargs)
        share(self)


def share(obj):
    emails = Email.objects.all()
    email_address = []
    for em in emails:
        email_address.append(em.email)

    email = EmailMessage()
    email.subject = obj.title
    print(obj.text)

    email.from_email = "ALEMSTROI <alemstroi.ru@gmail.com>"
    email.to = email_address
    email.attach(content=obj.text, mimetype="text/html")
    try:
        email.attach_file(obj.file.path)  # Attach a file directly
    except:
        pass

    email.send()
