# Create your views here.
import json
from django.core import serializers
from django.db.models import utils

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from product.views import sending_email

from .models import Contact, Email, CallMe

from django.core.mail import EmailMultiAlternatives


def list_contact(request):
    contacts = Contact.objects.all()
    json_contacts = serializers.serialize('json', contacts)
    json_contacts = json.loads(json_contacts)

    return JsonResponse({'contacts': json_contacts})


@csrf_exempt
def callMe(request):
    if request.method == 'POST':
        try:
            name = json.loads(request.body)['name']
            phoneNumber = json.loads(request.body)['phoneNumber']
            callDate = json.loads(request.body)['callDate']
        except:
            return JsonResponse({'message': 'Object is not correct'})

        CallMe.objects.create(name=name, phoneNumber=phoneNumber, callDate=callDate)

        subject = 'Связаться с нами'
        mess = \
            '''
                <h1 style="text-align:center"><span style="font-size:26px"><strong><span style="font-family:Georgia,serif"><cite>Связаться с нами:</cite></span></strong></span></h1>
                <table border="1" cellpadding="4" cellspacing="0" style="width:450px">
                    <tr>
                        <td>Имя:</td>
                        <th>{}</th>
                    </tr>
                    <tr>
                        <td>Номер Телефона :</td>
                        <th>{}</th>
                    </tr>
                    <tr>
                        <td>Когда:</td>
                        <th>{}</th>
                    </tr>
                </table>
            '''.format(name, phoneNumber, callDate)

        sending_email(mess, subject)

        return JsonResponse({'message': 'success'})


@csrf_exempt
def add_email(request):
    if request.method == 'POST':
        try:
            email = json.loads(request.body)['email']
        except:
            return JsonResponse({'message': 'Пустой объект'})

        e = Email.objects.filter(email=email)
        if e:
            return JsonResponse({'message': 'Вы уже подписались'})

        Email.objects.create(email=email)
        return JsonResponse({'message': 'Вы успешно подписались'})

#
# def share(request):
#     ob = Share.objects.all()[0]
#
#     subject, from_email, to = ob.title, 'timka.97.kg@mail.ru', 'abdymazhinov.timur@gmail.com'
#     text_content = 'This is an important message.'
#     msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
#     msg.attach_file(ob.doc.url)
#     msg.send()
#     return JsonResponse({'message': 'ok'})
