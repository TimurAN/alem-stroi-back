from django.urls import path
from .views import *


urlpatterns = [
    path('', list_contact),
    path('subscribe/', add_email),
    path('callme/', callMe)
]