from django.contrib import admin

# Register your models here.
from django.contrib import admin
from django import forms
from .models import *


class ViewContactAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ViewContactAdminForm, self).__init__(*args, **kwargs)

        self.fields['phoneNumber'].label = 'Номер телефона'
        self.fields['address'].label = 'Адрес'
        self.fields['email'].label = 'Эл. почта'
        self.fields['whatsApp'].label = 'Ватсап'
        self.fields['vk_url'].label = 'ВКонтакте'
        self.fields['ok_url'].label = 'Одноклассники'
        self.fields['instagram'].label = 'Инстаграм'


class ViewContact(admin.ModelAdmin):
    list_display = ["fphoneNumber", "faddress", "femail", "fwhatsApp", "fvk_url", "fok_url"]
    prepopulated_fields = {'whatsApp': ('phoneNumber', )}

    def fphoneNumber(self, obj):
        return obj.phoneNumber

    fphoneNumber.short_description = 'Номер телефона'

    def faddress(self, obj):
        return obj.address

    faddress.short_description = 'Адрес'

    def femail(self, obj):
        return obj.email

    femail.short_description = 'Эл. почта'

    def fwhatsApp(self, obj):
        return obj.whatsApp

    fwhatsApp.short_description = 'Ватсап'

    def fvk_url(self, obj):
        return obj.vk_url

    fvk_url.short_description = 'ВКонтакте'

    def fok_url(self, obj):
        return obj.ok_url

    fok_url.short_description = 'Одноклассники'

    def finstagram(self, obj):
        return obj.instagram

    finstagram.short_description = 'Инстаграм'

    search_fields = ['phoneNumber']
    form = ViewContactAdminForm


class ViewEmailAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ViewEmailAdminForm, self).__init__(*args, **kwargs)

        self.fields['email'].label = 'Эл.Почта'


class ViewEmail(admin.ModelAdmin):
    list_display = ["femail"]

    def femail(self, obj):
        return obj.email

    femail.short_description = 'Эл. почта'

    search_fields = ['email']
    form = ViewEmailAdminForm


class ViewDistributionAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ViewDistributionAdminForm, self).__init__(*args, **kwargs)

        self.fields['title'].label = 'Название рассылки'
        self.fields['text'].label = 'Описание рассылки'
        self.fields['file'].label = 'Загрузить Файл'


class ViewDistribution(admin.ModelAdmin):
    list_display = ["ftitle", "ftext", "ffile"]

    def ftitle(self, obj):
        return obj.title

    ftitle.short_description = 'Название рассылки'

    def ftext(self, obj):
        return obj.text

    ftext.short_description = 'Описание рассылки'

    def ffile(self, obj):
        return obj.file

    ffile.short_description = 'Прикрепленный файл'

    search_fields = ['title']
    form = ViewDistributionAdminForm


class ViewCallMe(admin.ModelAdmin):
    list_display = ["fname", "fphoneNumber", "fcallDate"]

    def fname(self, obj):
        return obj.name

    fname.short_description = 'Имя'

    def fphoneNumber(self, obj):
        return obj.phoneNumber

    fphoneNumber.short_description = 'Номер телефона'

    def fcallDate(self, obj):
        return obj.callDate

    fcallDate.short_description = 'Время звонка'

    search_fields = ['name']


admin.site.register(Contact, ViewContact),
admin.site.register(Email, ViewEmail),
admin.site.register(Distribution, ViewDistribution),
admin.site.register(CallMe, ViewCallMe),

